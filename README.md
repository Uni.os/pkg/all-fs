# all-fs

Have every major Filesystem kernel module & FUSE included by default! Makes it much easier for users migrating to UniOS to use.
- NTFS
- exFAT
- APFS
- HFS+
- F2FS
- ZFS